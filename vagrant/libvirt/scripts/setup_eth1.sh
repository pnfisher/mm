#!/bin/bash

route add default gw 192.168.1.1 >& /dev/null
#route -A inet6 add default gw fc00::1 eth1
eval $(route -n | awk '{ \
if ($8 == "eth0" && $2 != "0.0.0.0") print "route del default gw " $2; \
}')
