#!/bin/bash

if [ $# != 2 ]; then
  echo "setup_swap.sh: usage: device block_count"
  exit 1
fi

dev=$1
block_count=$2

cat /etc/fstab | egrep "[ 	]swap[ 	]" >& /dev/null
[ $? == 0 ] && echo "swap already set up" && exit 0

[ ! -b ${dev} ] && echo "setup_swap.sh: ${dev} missing" && exit 1

echo "partitioning ${dev}"
echo "0 ${block_count} S" | sfdisk ${dev}
[ $? != 0 ] && echo "sfdisk partitioning of ${dev} failed" && exit 1

echo "formatting swap disk"
mkswap ${dev}1
[ $? != 0 ] && echo "mkswap failed" && exit 1

echo "turning swap on"
echo "${dev}1 none swap sw 0 0" >> /etc/fstab
swapon ${dev}1
free -h
