#!/bin/bash

user=$1
pass=$(echo $2 | sed 's/\//\\\//g')
uid=$3
gid=$4
host=$5

fedora=
sudo_group=sudo
if [ -f /etc/fedora-release ]; then
   fedora=true
   sudo_group=wheel
fi

if [ -z "$user" -o -z "$pass" -o -z "$uid" -o -z "$gid" -o -z "$host" ]; then
  echo "$0: error, missing arguments"
  exit 1
fi

getent passwd ${user} >& /dev/null
if [ $? != 0 ]; then
  echo "adding user ${user}"
  useradd -m -d /home/${user} -G ${sudo_group} -g ${gid} -s /bin/bash ${user}
  usermod -o -u ${uid} ${user}
  echo "here are ${user}'s new settings"
  getent passwd ${user}
  # change ${user}'s password
  sed -i "s/^${user}:[^:]*:\(.*\)$/${user}:${pass}:\1/g" /etc/shadow
fi

newuid=1001
while true; do
  getent passwd ${newuid} >& /dev/null
  (($? != 0)) && break
  let newuid=newuid+1
done

# change vagrant uid if it clashes with new user (i.e. $uid)
vuid=$(id -u vagrant) 2> /dev/null
if [ "$vuid" == "$uid" ]; then
  echo "resetting vagrant uid to ${newuid}"
  # can't use 'usermod -u ${newuid} vagrant' because vagrant is
  # currently using uid 1000 and usermod won't allow us to switch it
  # out from under vagrant's feet
  sed -i \
      "s/^vagrant:x:${vuid}:/vagrant:x:${newuid}:/g" \
      /etc/passwd
  echo "here are vagrant's new settings"
  getent passwd vagrant
  # error check them as well
  getent passwd vagrant | egrep "^vagrant:x:${newuid}:" >& /dev/null
  (($? != 0)) && { echo "vagrant's settings are wrong"; exit 1; }
  # fix ownership of vagrant's home directory
  echo "chown -R vagrant /home/vagrant"
  chown -R vagrant /home/vagrant
  # we've stolen vagrant's uid, so vagrant is going to have problems
  # accessing /tmp/vagrant-shell, make it group readable/writeable to
  # fix this
  echo "chmod -R g+rw /tmp/vagrant-shell"
  chmod -R g+rw /tmp/vagrant-shell
fi

#
# tighten up security because we're going to mount ${user} home
# directory
#

# change vagrant's password (use $2, because we've munged the forward
# slashes in $pass)
getent passwd vagrant | grep "vagrant:$2:" >& /dev/null
if [ $? = 0 ]; then
  echo "giving vagrant the same password as ${user}"
  sed -i "s/^vagrant:[^:]*:\(.*\)$/vagrant:${pass}:\1/g" /etc/shadow
  # error check our fix
  getent shadow vagrant | grep "vagrant:$2:" >& /dev/null
  (($? != 0)) && { echo "vagrant's password incorrect"; exit 1; }
fi

# disable root password
getent passwd root | egrep "^root:\!:" >& /dev/null
if [ $? = 0 ]; then
  echo "disabling root password"
  usermod -p '!' root
  # error check our fix
  getent shadow root | egrep "^root:\!:" >& /dev/null
  (($? != 0)) && { echo "root's password not disabled"; exit 1; }
fi

# disable ubuntu password
getent passwd ubuntu | egrep "^ubuntu:\!:" >& /dev/null
if [ $? = 0 ]; then
  echo "disabling ubuntu password"
  usermod -p '!' ubuntu
  # error check our fix
  getent shadow ubuntu | egrep "^ubuntu:\!:" >& /dev/null
  (($? != 0)) && { echo "ubuntu's password not disabled"; exit 1; }
fi

# allow ${user} to sudo with no password prompt
regexp="${user}.*NOPASSWD"
cat /etc/sudoers | egrep "${regexp}" >& /dev/null
if [ $? != 0 ]; then
  echo "tweaking /etc/sudoers for user ${user}"
  echo "${user} ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
fi

subline="${user}:100000:65536"

# clear out any pre-existing subuid mappings
if [ -f /etc/subuid ]; then
  echo "setup pre-existing /etc/subuid with just ${subline}"
  echo "${subline}" | tee /etc/subuid > /dev/null
fi

# clear out any pre-existing subgid mappings
if [ -f /etc/subgid ]; then
  echo "setup pre-existing /etc/subgid with just ${subline}"
  echo "${subline}" | tee /etc/subgid > /dev/null
fi

if [ ! -z "$fedora" ]; then
  echo "preparing fedora guest (be patient please)"
  # dnf -y distro-sync
  dnf -y install python nfs-utils nano
  setsebool -P use_nfs_home_dirs 1
  (cd /etc && ln -sf ../usr/share/zoneinfo/Canada/Eastern localtime)
  # (cd /etc && ln -sf ../usr/share/zoneinfo/Etc/GMT+4 localtime)
  regexp="vagrant.*NOPASSWD"
  cat /etc/sudoers | egrep "${regexp}" >& /dev/null
  if [ $? != 0 ]; then
    echo "tweaking /etc/sudoers for user vagrant"
    echo "vagrant ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
  fi
else
  echo "preparing ubuntu guest"
  apt-get -y install nfs-common python nano  
fi
