#!/bin/bash

spfile="shadowpass.txt"
shadowpass=$(cat ${spfile} 2> /dev/null | tail -1)
[ -n "${shadowpass}" ] && exit 0
while true; do
  read -s -p "Password for ${USER} on vagrant machines: " p1
  echo ""
  read -s -p "Reenter password for ${USER} on vagrant machines: " p2
  echo ""
  [ "$p1" == "$p2" -a -n "$p1" ] && break
  echo "mismatch, please try again"
done
rm -f ${spfile}
echo "# mkpasswd password -m sha-512" > ${spfile}
chmod 0600 ${spfile}
mkpasswd "${p1}" -m sha-512 >> ${spfile}
