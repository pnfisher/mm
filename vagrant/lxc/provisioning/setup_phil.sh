#!/bin/bash

user=phil

regexp="^${user}:"
cat /etc/passwd | egrep "${regexp}" > /dev/null
if [ $? != 0 ]; then
  echo "adding user ${user}"
  useradd -m -d /home/${user} -G sudo -g 100 -s /bin/bash ${user}
  usermod -o -u 1000 ${user}
  echo "${user}:${user}" | chpasswd 2> /dev/null
fi

regexp="${user}.*NOPASSWD"
cat /etc/sudoers | egrep "${regexp}" >& /dev/null
if [ $? != 0 ]; then
  echo "tweaking /etc/sudoers for user ${user}"
  echo "${user} ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
fi

# regexp="[ 	]/home/${user}[ 	]"
# cat /etc/fstab | egrep "${regexp}" >& /dev/null
# if [ $? != 0 ]; then
#   echo "mounting user ${user} home directory"
#   echo "echo:/home/${user} /home/${user} nfs4 soft,intr 0 0" >> /etc/fstab
#   mount -a
# fi

# change vagrant uid from 1000 to 2000
uid=`id -u vagrant` 2> /dev/null
if [ "$uid" == 1000 ]; then
  echo "resetting vagrant uid to 2000"
  sed -i 's/^vagrant:x:1000:/vagrant:x:2000:/g' /etc/passwd
  echo "chown -R vagrant /home/vagrant"
  chown -R vagrant /home/vagrant
  # we've stolen vagrant's uid, so vagrant is going to have problems
  # accessing /tmp/vagrant-shell, make it group readable/writeable to
  # fix this
  echo "chmod -R g+rw /tmp/vagrant-shell"
  chmod -R g+rw /tmp/vagrant-shell
fi
