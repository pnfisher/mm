ENV['VAGRANT_DEFAULT_PROVIDER'] = "lxc"

Vagrant.configure("2") do |config|

  #
  # ssh settings
  #

  # suppress annoying stdin is not a tty message on all machines
  config.ssh.shell = "bash -c 'BASH_ENV=/etc/profile exec bash'"
  # support X11 forwarding
  config.ssh.forward_x11 = "true"

  #
  # global provisioning for all machines
  #
  config.vm.provision "shell",
                      path: "provisioning/setup_phil.sh",
                      keep_color: true,
                      name: "setup_phil.sh (with uid 1000, vagrant gets 2000)"

  #
  # machines
  #

  ## albert
  config.vm.define "albert" do |machine|

    machine.vm.hostname = "albert"

    system("./provisioning/setup_fstab.sh albert")

    config.vm.provider :lxc do |lxc|
      lxc.customize "utsname", "albert"
      lxc.customize "network.link", "br0"
      #lxc.customize "mount", "/home/phil/mm/vagrant/lxc/provisioning/fstab"
    end

    # turn off default syncing
    machine.vm.synced_folder ".", "/vagrant", disabled: true

    # It should be an Ubuntu 14.04 box
    machine.vm.box = "fgrehm/trusty64-lxc"

  end

end
