#!/bin/bash

which vagrant >& /dev/null
(($? != 0)) && {
  echo "error, vagrant isn't installed" 1>&2;
  exit 1;
}

backup=backup
prefix=libvirt
dir=/home/phil/mm/vagrant/${prefix}

cd ${dir} || {
  echo "error, ${dir} missing" 1>&2
  exit 1
}

rm -rf ~/.ansible

if [ "$1" == "rebuild" ]; then
  if [ -z "$2" ]; then
    echo "error, missing reup target machine argument" 1>&2
    exit 1
  fi
  shift
  vagrant destroy $* || exit 1
  vagrant up $* || exit 1
  exit 0
fi

if [ "$1" == "backup" ]; then
  if [ -z "$2" ]; then
    echo "error, missing backup target machine argument" 1>&2
    exit 1
  fi
  dir=${backup}/$2
  if [ -z "$3" ]; then
    echo \
      "error, missing tag (e.g. 'basic' or 'base') for the backup image" 1>&2
    exit 1
  fi
  dst=${dir}/$3.img
  src=/var/lib/${prefix}/vagrant/${prefix}_$2.img
  if [ ! -f ${src} ]; then
    echo "error, ${src} is missing" 1>&2
    exit 1
  fi
  vagrant halt $2 || exit 1
  mkdir -p ${dir}
  echo "sorry, we must use sudo to create the backup"
  sudo bash -e <<EOF
  #qemu-img convert -p -f qcow2 -O qcow2 ${src} ${dst}
  rsync -ah --sparse --progress ${src} ${dst}
  chown ${USER}:${USER} ${dst}
  chmod 0600 ${dst}
  ls -lh ${dst}
EOF
  if (($? != 0)); then
    echo "error, backup of ${src} failed" 1>&2
    exit 1
  fi
  exit 0
fi

if [ "$1" == "restore" ]; then
  if [ -z "$2" ]; then
    echo "error, missing restore target machine argument" 1>&2
    exit 1
  fi
  dir=${backup}/$2
  if [ ! -d ${dir} ]; then
    echo "error, ${dir} directory is missing" 1>&2
    exit 1
  fi
  if [ -z "$3" ]; then
    echo "error, missing tag name for the backup image" 1>&2
    echo "current tags are:" 1>&2
    echo "-----------------" 1>&2
    for b in ${dir}/*.img; do
      basename $b .img 1>&2
    done
    exit 1
  fi
  src=${dir}/$3.img
  dst=/var/lib/${prefix}/vagrant/${prefix}_$2.img
  if [ ! -f ${src} ]; then
    echo "error, ${src} is missing" 1>&2
    exit 1
  fi
  if [ ! -f ${dst} ]; then
    echo "error, ${dst} is missing" 1>&2
    echo -e "use\n  vagrant up $1 --no-provision\nand then rerun restore" 1>&2
    exit 1
  fi
  vagrant halt $2 || exit 1
  echo "sorry, we must use sudo to restore the backup"
  sudo bash -e <<EOF
  #qemu-img convert -p -f qcow2 -O qcow2 ${src} ${dst}
  rsync -ah --sparse --progress ${src} ${dst}
  sudo chmod 0600 ${dst}
  ls -lh ${dst}
EOF
  if (($? != 0)); then
    echo "error, restore of ${dst} failed" 1>&2
    exit 1
  fi
  exit 0
fi

if [ "$1" == "snapshot" ]; then
  if [ -z "$2" ]; then
    echo "error, missing snapshot target machine argument" 1>&2
    exit 1
  fi
  dst=/var/lib/${prefix}/vagrant/${prefix}_$2.img
  if [ ! -f ${dst} ]; then
    echo "error, ${dst} is missing" 1>&2
    exit 1
  fi
  vagrant halt $2 || exit 1
  echo "sorry, we must use sudo to process the snapshot"
  if [ -z "$3" ]; then
    sudo qemu-img snapshot -l ${dst}
    if (($? != 0)); then
      echo "error, snapshot -l ${dst} failed" 1>&2
      exit 1
    fi
    exit 0
  fi
  while read id tag stuff; do
    if [ "$tag" == "$3" ]; then
      echo "tag '${tag}' already exists"
      echo "applying snapshot id '${tag}' to ${dst}"
      sudo qemu-img snapshot -a ${tag} ${dst}
      if (($? != 0)); then
        echo "error, snapshot -a ${tag} ${dst} failed" 1>&2
        exit 1
      fi
      exit 0
    fi
  done < <(sudo qemu-img snapshot -l ${dst})
  echo "creating snapshot id '$3' in ${dst}"
  sudo qemu-img snapshot -c $3 ${dst}
  if (($? != 0)); then
    echo "error, snapshot -c $3 ${dst} failed" 1>&2
    exit 1
  fi
  exit 0
fi

vagrant $*
