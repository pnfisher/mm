#!/bin/bash

if [ ! -f ../scripts/helper_funcs.sh ]; then
  echo "error, can't find helper_funcs.sh"
  exit 1
else
  . ../scripts/helper_funcs.sh
  check_vf
fi

pool="vagrant"

echo vagrant destroy
vagrant destroy
#[ $? != 0 ] && exit 0

paths=`virsh vol-list --pool ${pool} | \
      egrep "\.img|\.raw|\.qcow2" | \
      sed 's/[ ][ ]*/ /g' | \
      sed 's/^ //g' | \
      cut -d ' ' -f 1`

if [ -z "$paths" ]; then
  echo "No image(s) in ${pool}'s libvirt pool to delete, exiting"
  exit 0
fi

for p in $paths; do

  echo virsh vol-delete $p
  virsh vol-delete --pool ${pool} $p

done

