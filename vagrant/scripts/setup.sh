#!/bin/bash

dir=/home/phil/src/mm/vagrant
cd ${dir} || {
  echo "${dir} is missing" 1>&2;
  exit 1;
}

if [ ! -f scripts/helper_funcs.sh ]; then
  echo "error, can't find helper_funcs.sh" 1>&2
  exit 1
else
  . scripts/helper_funcs.sh
fi

#scripts=$(find `pwd`/ -type f -name "v*.sh")
#for s in ${scripts}; do\
#  (cd ~/bin; ln -sf $s $(basename $s .sh))
#done

vlibvirt=$(pwd)/scripts/vlibvirt.sh
if [ -x ${vlibvirt} ]; then
  (cd ~/bin; ln -sf ${vlibvirt} vlibvirt)
else
  echo "couldn't find executable called ${vlibvirt}" 1>&2
  exit 1
fi

if [ ! -f libvirt/pool.xml ]; then
  echo "error, libvirt/pool.xml is missing" 1>&2
  exit 1
fi

pooldir=/var/lib/libvirt/vagrant
if [ ! -d ${pooldir} ]; then
  echo "warning, ${pooldir} directory missing"
  echo "  going to use sudo to create the directory"
  sudo bash <<EOF
    mkdir ${pooldir}
    chmod g+r-w,o-rw ${pooldir}
EOF
  if (($? != 0)); then
    echo "error, failed to create ${pooldir}" 1>&2
    exit 1
  fi
fi

pool=libvirt/pool.xml
if [ ! -f ${pool} ]; then
  echo "error, ${pool} is missing" 1>&2
  exit 1
fi

virsh pool-create ${pool} >& /dev/null
if (($? != 0)); then
  echo "warning, virsh pool-create failed" 1>&2
  virsh pool-list --all
  exit 0
fi

virsh pool-start vagrant >& /dev/null
if (($? != 0)); then
  echo "warning, virsh pool-start failed" 1>&2
  virsh pool-list --all
  exit 0
fi

virsh pool-autostart vagrant >& /dev/null
if (($? != 0)); then
  echo "warning, virsh pool-autostart failed" 1>&2
  virsh pool-list --all
  exit 0
fi

virsh pool-list --all

exit 0

#for d in libvirt lxc; do
for d in libvirt; do

  cd ${d} || {
    echo "{d} is missing" 1>&2;
    exit 1;
  }

  vf=Vagrantfile.$(hostname).rb
  if [ ! -f ${vf} ]; then
    echo "error, ${vf} is missing in directory ${d}" 1>&2
    exit 1
  fi
  ln -sf ${vf} Vagrantfile

  # ansible="../../ansible"
  # if [ ! -d $ansible ]; then
  #   echo "error, $ansible is missing"
  #   exit 1
  # fi
  # ln -sf $ansible .

done
