#!/bin/bash

cd /home/phil/src/mm/vagrant/qemu

if [ ! -f ../scripts/helper_funcs.sh ]; then
  echo "error, can't find helper_funcs.sh"
  exit 1
else
  . ../scripts/helper_funcs.sh
  check_vf
fi

vagrant global-status $*
