#!/bin/bash

cd /home/phil/src/mm/vagrant/qemu

if [ ! -f ../scripts/helper_funcs.sh ]; then
  echo "error, can't find helper_funcs.sh"
  exit 1
else
  . ../scripts/helper_funcs.sh
  check_vf
  check_arg $*
fi

machines=${machine}
if [ -z "${machines}" ]; then
  machines=`vagrant status | grep libvirt | cut -f 1 -d ' '`
fi

for m in ${machines}; do
  vagrant destroy $m
  ssh-keygen -f "/home/phil/.ssh/known_hosts" -R $m >& /dev/null
done

