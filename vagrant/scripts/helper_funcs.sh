check_vf() {

  if [ ! -e Vagrantfile ]; then
    echo "error, Vagrantfile is missing"
    exit 1
  fi

}

machine=

check_arg() {

  [ $# != 1 ] && echo "missing machine name|all" && exit 1
  machine=$1
  [ ${machine} == "all" ] && machine=

}
