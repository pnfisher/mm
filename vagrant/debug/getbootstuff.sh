#!/bin/bash

machine=$1
verstr=$2
vfilesh=${machine}/lx_ver.sh
vfilemk=${machine}/lx_ver.mk
srcdir=${machine}/src
extra="debug"
current=
image=/var/lib/libvirt/vagrant/libvirt_$1.img

do_sudo() {
  if [ ! -f ${image} ]; then
    echo "error, ${image} is missing" 1>&2
    exit 1
  fi
  sudo bash -e <<EOF
  cd ${machine} || exit 1
  if [ ! -f $1.img ]; then
  echo "fetching image for $1"
  #qemu-img convert -p -f qcow2 -O qcow2 ${image} $1.img
  rsync -ah --progress --sparse ${image} $1.img
  fi
  echo "mounting $1.img"
  modprobe nbd max_part=63
  mkdir -p ./mnt >& /dev/null
  qemu-nbd -c /dev/nbd0 $1.img
  mount /dev/nbd0p1 ./mnt
  echo "extracting from $1.img"
  cp mnt/etc/lsb-release .
  cp mnt/boot/initrd.img-* .
  cp mnt/boot/config-* .
  rm -f module.list
  cd mnt || exit 1
  find lib/modules/ -name "*.ko" | grep -v "/updates/" > ../module.list
EOF
  rc=$?
  # no matter what happened, let's clean up
  sudo bash <<EOF
  cd ${machine} || exit 1
  echo "umounting $1.img"
  umount ./mnt >& /dev/null
  qemu-nbd -d /dev/nbd0 >& /dev/null
  #killall qemu-nbd >& /dev/null
  rmdir ./mnt >& /dev/null
  chown -R phil:users .
EOF
  if ((rc != 0)); then
    echo "error, attempt to fetch ${machine} stuff failed" 1>&2
    exit 1
  fi
}

if [ -z "${machine}" ]; then
  echo "error, you must specify a target host" 1>&2
  exit 1
fi

if [ -z "${verstr}" ]; then
  cat <<EOF 1>&2
error, you must specify a version string
legal version string options are:
--------------------------------
current   (use default version string of installed image kernel)
latest    (use default version string of apt-get source)
specified (specify a specific version string)
EOF
  exit 1
fi

if [ "${verstr}" == "current" ]; then
  verstr=
  current=1
elif [ "${verstr}" == "latest" ]; then
  verstr=
fi

mkdir -p ${machine}
if [ ! -f ${machine}/${machine}.img ]; then
  echo "sorry, need to use sudo in order to populate ${machine} directory"
  do_sudo ${machine}
fi

count=$(ls ${machine}/initrd.img-* 2> /dev/null | wc -l)
if (($? != 0)) || ((count == 0)); then
  echo "sorry, need to use sudo in order to populate ${machine} directory"
  do_sudo ${machine}
fi

if [ ! -f ${machine}/modules.list ]; then
  echo "sorry, need to use sudo in order to populate ${machine} directory"
  do_sudo ${machine}
fi

if [ -z "$verstr" ]; then
  count=$(ls ${machine}/initrd.img-* 2> /dev/null | wc -l)
  if (($? != 0)) || ((count != 1)); then
    echo "error, wrong number of initrd.img files in ${machine} directory" 1>&2
    ls -1 ./${machine}/initrd.img* 1>&2
    echo "please specify a full verion string as second argument" 1>&2
    exit 1
  fi
  verstr=$(ls ${machine}/initrd.img-* | sed 's/.*initrd.img-\(.*\)$/\1/g')
  if (($? != 0)) || [ -z "$verstr" ]; then
    echo "error, can't figure out the correct linux version" 1>&2
    exit 1
  fi
fi

if [ ! -f ${machine}/initrd.img-${verstr} ]; then
  echo "error, ${machine}/initrd.img-${verstr} is missing" 1>&2
  exit 1
fi

if [ ! -f ${machine}/config-${verstr} ]; then
  echo "error, ${machine}/config-${verstr} is missing" 1>&2
  exit 1
fi

echo "fetching linux-image-${verstr} source"
mkdir -p ${srcdir}
(cd ${srcdir} && apt-get source linux-image-${verstr})
if (($? != 0)); then
  echo "error, apt-get source linux-image-${verstr} failed"
  exit 1
fi

src=$(echo ${srcdir}/linux-*/ 2> /dev/null)
count=$(ls -1d ${srcdir}/linux-*/ | wc -l)
if ((count != 1)); then
  echo "error, unable to determine true linux source directory:" 1>&2
  for d in $(echo ${srcdir}/linux-*); do
    echo "  "$d 1>&2
  done
  exit 1
fi

ln -sf $(pwd)/${machine}/config-${verstr} ${src}/.config
if (($? != 0)); then
  echo "error, ln -sf ${machine}/config-${verstr} ${src}/.config failed"
  exit 1
fi

rm -f ${vfilesh} ${vfilemk}

touch ${vfilesh} ${vfilemk}

if [ -z "$current" ]; then

  for v in VERSION PATCHLEVEL SUBLEVEL; do
    cat ${src}/Makefile | egrep "^$v = " | sed 's/ = /=/g' >> ${vfilesh}
    cat ${src}/Makefile | egrep "^$v = " >> ${vfilemk}
  done

  for v in EXTRAVERSION; do
    echo $(cat ${src}/Makefile | egrep "^$v = " | \
                sed 's/ = \(.*\)/=\1/g')${extra} >> ${vfilesh}
    echo $(cat ${src}/Makefile | egrep "^$v = " | \
                sed 's/ = \(.*\)/ = \1/g')${extra} >> ${vfilemk}
  done

else

  version=$(echo $verstr | sed 's/\([0-9]*\)\.[0-9]*\.[0-9]*.*/\1/g')
  patchlevel=$(echo $verstr | sed 's/[0-9]*\.\([0-9]*\)\.[0-9]*.*/\1/g')
  sublevel=$(echo $verstr | sed 's/[0-9]*\.[0-9]*\.\([0-9]*\).*/\1/g')
  extraversion=$(echo $verstr | sed 's/[^-]*\(-.*\)/\1/g')

  echo VERSION=${version} > ${vfilesh}
  echo PATCHLEVEL=${patchlevel} >> ${vfilesh}
  echo SUBLEVEL=${sublevel} >> ${vfilesh}
  echo EXTRAVERSION=${extraversion} >> ${vfilesh}

  echo VERSION ?= ${version} > ${vfilemk}
  echo PATCHLEVEL ?= ${patchlevel} >> ${vfilemk}
  echo SUBLEVEL ?= ${sublevel} >> ${vfilemk}
  echo EXTRAVERSION ?= ${extraversion} >> ${vfilemk}

  (cd ${machine} && ln -sf initrd.img-${verstr} initrd.img)
  if (($? != 0)); then
    echo "error, ln -sf initrd.img-${verstr} initrd.img failed" 1>&2
    exit 1
  fi

fi

echo LINUX_SRC=$(pwd)/${src} >> ${vfilesh}
echo LINUX_SRC = $(pwd)/${src} >> ${vfilemk}
echo "here is your setup:"
echo "------------------"
cat ${vfilemk}
cat ${vfilesh}
