#!/bin/bash

if [ -z "$1" ]; then
  echo "error, host argument missing" 1>&2
  exit 1
fi

cd $1 || {
  echo "error, $1 directory missing" 1>&2;
  exit 1;
}

if [ ! -f $1.img ]; then
  echo "error, $1.img missing" 1>&2
  exit 1
fi

sudo bash <<EOF
modprobe nbd max_part=63
mkdir -p ./mnt >& /dev/null
qemu-nbd -c /dev/nbd0 $1.img
mount /dev/nbd0p1 ./mnt
EOF
