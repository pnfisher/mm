#!/bin/bash

if [ -z "$1" ]; then
  echo "error, no target host specified" 1>&2
  exit 1
fi

if [ ! -d "$1" ]; then
  echo "error, directory $1 is missing" 1>&2
  exit 1
fi

# . $1/lx_ver.sh || {
#   echo "error, failed to source $1/lx_ver.sh" 1>&2;
#   exit 1;
# }

image=$1/$1.img
initrd=$1/initrd.img

if [ ! -f ${image} ]; then
  echo "error, ${image} missing" 1>&2
  exit 1
fi

if [ -z "$2" ]; then
  echo "error, target argument missing, must be one of:" 1>&2
  echo "  bzImage  (custom bzImage without -s)" 1>&2
  echo "  debug    (custom bzImage with -s)" 1>&2
  echo "  image    (no -kernel, no -initrd)" 1>&2
  echo "  original (same as image)" 1>&2
  exit 1
fi

if [ "$2" == "bzImage" -o "$2" == "debug" ]; then
  kernel=$1/bzImage
  for f in ${kernel} ${initrd}; do
    if [ ! -f $f ]; then
      echo "error, $f missing" 1>&2
      exit 1
    fi
  done
fi

i=0
args[((i++))]="-enable-kvm"
#args[((i++))]="-name libvirt_crazyhorse"
args[((i++))]="-machine pc-i440fx-trusty,accel=kvm,usb=off"
args[((i++))]="-m 4096"
args[((i++))]="-realtime mlock=off"
args[((i++))]="-uuid b1cfad91-7649-4105-8ce1-0a05277c53c0"
args[((i++))]="-no-user-config"
args[((i++))]="-nodefaults"
args[((i++))]="-rtc base=utc"
#args[((i++))]="-no-shutdown"
args[((i++))]="-boot strict=on"
args[((i++))]="-device piix3-usb-uhci,id=usb,bus=pci.0,addr=0x1.0x2"
args[((i++))]="-device ahci,id=ahci0,bus=pci.0,addr=0x3"
args[((i++))]="-drive file=${image},if=none,id=drive-sata0-0-0,format=qcow2"
args[((i++))]="-device ide-hd,bus=ahci0.0,drive=drive-sata0-0-0,id=sata0-0-0,bootindex=1"
args[((i++))]="-netdev tap,helper=/usr/lib/qemu-bridge-helper,id=hostnet0"
args[((i++))]="-device virtio-net-pci,netdev=hostnet0,id=net0,mac=52:54:00:9d:8f:3f,bus=pci.0,addr=0x5"
args[((i++))]="-netdev tap,helper=/usr/lib/qemu-bridge-helper,id=hostnet1"
args[((i++))]="-device virtio-net-pci,netdev=hostnet1,id=net1,mac=52:54:00:c5:22:31,bus=pci.0,addr=0x6"
#args[((i++))]="-chardev pty,id=charserial0"
#args[((i++))]="-device isa-serial,chardev=charserial0,id=serial0"
args[((i++))]="-k en-us"
#args[((i++))]="-device cirrus-vga,id=video0,bus=pci.0,addr=0x2"
args[((i++))]="-device virtio-balloon-pci,id=balloon0,bus=pci.0,addr=0x4"
#args[((i++))]="-serial telnet::5000,server,wait"
args[((i++))]="-nographic"

if [ "$2" == "bzImage" ]; then
  args[((i++))]="-serial telnet::5000,server,wait"
  set -x
  qemu-system-x86_64 \
      ${args[@]} \
      -append "root=/dev/sda1 console=ttyS0" \
      -kernel ${kernel} \
      -initrd ${initrd}
  exit $?
fi

if [ "$2" == "debug" ]; then
  args[((i++))]="-serial telnet::5000,server,nowait"
  set -x
  qemu-system-x86_64 \
      ${args[@]} \
      -append "root=/dev/sda1 console=ttyS0" \
      -kernel ${kernel} \
      -initrd ${initrd} \
      -s
  exit $?
fi

if [ "$2" == "original" -o "$2" == "image" ]; then
  args[((i++))]="-serial telnet::5000,server,wait"
  set -x
  qemu-system-x86_64 ${args[@]}
  exit $?
fi

echo "error, second argument makes no sense" 1>&2
exit 1


