#!/bin/bash

if [ -z "$1" ]; then
  echo "error, host argument missing" 1>&2
  exit 1
fi

cd $1 || {
  echo "error, $1 directory missing" 1>&2;
  exit 1;
}

if [ ! -d mnt ]; then
  echo "error, mnt directory missing" 1>&2;
  exit 1;  
fi

sudo bash <<EOF
umount ./mnt >& /dev/null
qemu-nbd -d /dev/nbd0 >& /dev/null
rmdir ./mnt >& /dev/null
EOF
