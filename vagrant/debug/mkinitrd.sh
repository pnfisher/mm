#!/bin/bash

error_msg() {
  echo "error, $*" 1>&2
}

test_rc() {
  if (($1 != 0)); then
    error_msg $2
    exit $1
  fi
}

if [ -z "$1" ]; then
  error_msg "missing target host argument"
  exit 1
fi

machine=$1
cd ${machine} || {
  error_msg "${machine} directory missing";
  exit 1;
}

. lx_ver.sh || {
  error_msg "couldn't souce ${machine}/lx_ver.sh";
  exit 1;
}

newver="${VERSION}.${PATCHLEVEL}.${SUBLEVEL}${EXTRAVERSION}"
oldver=

for f in $(ls initrd.img-*); do
  [ "$f" == "initrd.img-${newver}" ] && continue
  if [ -n "$oldver" ]; then
    error_msg "can't figure out which initrd.img is the original"
    ls -1 initrd.img-* 1>&2
    exit 1
  fi
  oldver=$(echo $f | sed 's/initrd.img-//g')
done

if [ -z "$oldver" ]; then
  error_msg "original initrd.img is missing"
  ls -1 initrd.img-* 1>&2
  exit 1
fi

old_initrd="initrd.img-${oldver}"
linuxdir="$LINUX_SRC"
test -d ${linuxdir}
test_rc $? "${linuxdir} is missing"

grep ${oldver} ${linuxdir}/../*.dsc >& /dev/null
if [ $? != 0 ]; then
  error_msg \
    "initrd.img-${oldver} doesn't appear to be the truly original initrd.img"
  exit 1
fi

ramfsdir="initramfs"
bzImage="${linuxdir}/arch/x86_64/boot/bzImage"
targets="bzImage modules"

if [ -n "$2" -a "$2" == "make" ]; then
  echo "making ${targets}"
  make -C ${linuxdir} EXTRAVERSION=${EXTRAVERSION} ARCH=x86_64 -j8 ${targets}
  test_rc $? "make bzImage failed"
  test -f ${bzImage}
  test_rc $? "${bzImage} missing"
  ln -sf ${bzImage} bzImage
  test_rc $? "ln -sf ${bzImage} bzImage failed"
fi

echo "extracting ${old_initrd}"
rm -rf ${ramfsdir}
mkdir ${ramfsdir} || exit 1
(cd ${ramfsdir}; gzip -d --stdout ../${old_initrd} | cpio -i)
test_rc $? "couldn't populate ${ramfsdir} directory"

moddir=${ramfsdir}/lib/modules/${oldver}
firmdir=${ramfsdir}/lib/firmware/${oldver}

test -d ${moddir}
test_rc $? "${moddir} is missing}"
test -d ${firmdir}
test_rc $? "${firmdir} is missing}"

mv ${moddir} $(dirname ${moddir})/${newver}
moddir=${ramfsdir}/lib/modules/${newver}
mv ${firmdir} $(dirname ${firmdir})/${newver}
firmdir=${ramfsdir}/lib/firmware/${newver}

test -d ${moddir}
test_rc $? "${moddir} is missing}"
test -d ${firmdir}
test_rc $? "${firmdir} is missing}"


echo "repopulating ${ramfsdir} with stripped and signed modules"

#for f in $(find ${moddir} -type f -print); do
for f in $(find ${moddir} -name "*.ko" -type f -print); do

  #newitem=${linuxdir}/debian/linux-image-${newver}/$(echo $f | cut -d'/' -f 2-)
  newitem=${linuxdir}/$(echo $f | cut -d'/' -f 6-)
  test -e ${newitem}
  test_rc $? "${newitem} is missing"

  #ln -sf ${newitem} $f
  #test_rc $? "ln -sf ${newitem} $f failed"
  cp ${newitem} $f
  test_rc $? "cp ${newitem} $f failed"

  strip --strip-debug $f
  test_rc $? "strip $f failed"

  # sign the module
  perl \
      ${linuxdir}/scripts/sign-file "sha512" \
      ${linuxdir}/signing_key.priv \
      ${linuxdir}/signing_key.x509 \
      $f
  test_rc $? "module signing failed"

done

#for f in $(find ${firmdir} -type f -print); do
for f in $(find ${firmdir} -name "*.bin" -type f -print); do

  #newitem=${linuxdir}/debian/linux-image-${newver}/$(echo $f | cut -d'/' -f 2-)
  newitem=${linuxdir}/firmware/$(echo $f | cut -d'/' -f 5-)
  test -e ${newitem}
  test_rc $? "${newitem} is missing"

  #ln -sf ${newitem} $f
  #test_rc $? "ln -sf ${newitem} $f failed"
  cp ${newitem} $f
  test_rc $? "cp ${newitem} $f failed"

done


echo "running depmod"
fakeroot -- depmod -F${linuxdir}/System.map -b $(pwd)/${ramfsdir} ${newver}


echo "creating ${machine}/initrd.img"
(cd ${ramfsdir}; find . -print | cpio -L -H newc -o | gzip > ../initrd.img)
test_rc $? "attempt to create new initrd.img failed"
ls -lh initrd.img

