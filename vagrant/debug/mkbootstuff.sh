#!/bin/bash

error_msg() {
  echo "error, $*" 1>&2
}

test_rc() {
  if (($1 != 0)); then
    error_msg $2
    exit $1
  fi
}

if [ -n "$1" ]; then
  error_msg "missing target host argument"
  exit 1
fi

machine=$1

. ${machine}/lx_ver.sh || {
  error_msg "couldn't souce ${machine}/lx_ver.sh";
  exit 1;
}

#EXTRAVERSION="-ckt25crazyhorse"
oldver="3.13.0-63-generic"
newver="3.13.11${EXTRAVERSION}"
old_initrd="initrd.img-${oldver}"
linuxdir="$(pwd)/src/linux-3.13.0"
ramfsdir="initramfs"
bzImage="${linuxdir}/arch/x86/boot/bzImage"

test -f ${old_initrd}
test_rc $? "${old_initrd} is missing"
test -d ${linuxdir}
test_rc $? "${linuxdir} is missing"

targets="bzImage modules"

echo "making ${targets}"
make -C ${linuxdir} EXTRAVERSION=${EXTRAVERSION} ARCH=x86_64 -j8 ${targets}
test_rc $? "make bzImage failed"
test -f ${bzImage}
test_rc $? "${bzImage} missing"

ln -sf ${bzImage} bzImage
test_rc $? "ln -sf ${bzImage} bzImage failed"


echo "extracting ${old_initrd}"
rm -rf ${ramfsdir}
mkdir ${ramfsdir} || exit 1
(cd ${ramfsdir}; gzip -d --stdout ../${old_initrd} | cpio -i)
test_rc $? "couldn't populate ${ramfsdir} directory"

moddir=${ramfsdir}/lib/modules/${oldver}
firmdir=${ramfsdir}/lib/firmware/${oldver}

test -d ${moddir}
test_rc $? "${moddir} is missing}"
test -d ${firmdir}
test_rc $? "${firmdir} is missing}"

mv ${moddir} $(dirname ${moddir})/${newver}
mv ${firmdir} $(dirname ${firmdir})/${newver}

moddir=${ramfsdir}/lib/modules/${newver}
firmdir=${ramfsdir}/lib/firmware/${newver}

test -d ${moddir}
test_rc $? "${moddir} is missing}"
test -d ${firmdir}
test_rc $? "${firmdir} is missing}"


echo "repopulating ${ramfsdir} with stripped and signed modules"

#for f in $(find ${moddir} -type f -print); do
for f in $(find ${moddir} -name "*.ko" -type f -print); do

  #newitem=${linuxdir}/debian/linux-image-${newver}/$(echo $f | cut -d'/' -f 2-)
  newitem=${linuxdir}/$(echo $f | cut -d'/' -f 6-)
  test -e ${newitem}
  test_rc $? "${newitem} is missing"

  #ln -sf ${newitem} $f
  #test_rc $? "ln -sf ${newitem} $f failed"
  cp ${newitem} $f
  test_rc $? "cp ${newitem} $f failed"

  strip --strip-debug $f
  test_rc $? "strip $f failed"

  # sign the module
  perl \
      ${linuxdir}/scripts/sign-file "sha512" \
      ${linuxdir}/signing_key.priv \
      ${linuxdir}/signing_key.x509 \
      $f
  test_rc $? "module signing failed"
  
done

#for f in $(find ${firmdir} -type f -print); do
for f in $(find ${firmdir} -name "*.bin" -type f -print); do

  #newitem=${linuxdir}/debian/linux-image-${newver}/$(echo $f | cut -d'/' -f 2-)
  newitem=${linuxdir}/firmware/$(echo $f | cut -d'/' -f 5-)
  test -e ${newitem}
  test_rc $? "${newitem} is missing"

  #ln -sf ${newitem} $f
  #test_rc $? "ln -sf ${newitem} $f failed"
  cp ${newitem} $f
  test_rc $? "cp ${newitem} $f failed"

done


echo "running depmod"
fakeroot -- depmod -F${linuxdir}/System.map -b $(pwd)/${ramfsdir} ${newver}


echo "creating ${bootdir}/initrd.img"
(cd ${ramfsdir}; find . -print | cpio -L -H newc -o | gzip > ${bootdir}/initrd.img)
test_rc $? "attempt to create new initrd.img failed"
ls -lh initrd.img

