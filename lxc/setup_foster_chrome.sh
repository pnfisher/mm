#!/bin/bash

clonee=foster-base
guest=foster-chrome
. helper_funcs.sh

lxc_clone
wait_for_guest 30

sudo -u phil env guest=${guest} bash -x <<EOF1
ssh-keygen -f "/home/phil/.ssh/known_hosts" -R ${guest}
ssh ${guest} sudo bash -x <<EOF2
wget -q -O - \
https://dl-ssl.google.com/linux/linux_signing_key.pub | \
sudo apt-key add -
bash -c \
'echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> \
/etc/apt/sources.list.d/google-chrome.list'
apt-get update
apt-get install -y google-chrome-stable < /dev/null
EOF2
EOF1

lxc-stop -n ${guest}
