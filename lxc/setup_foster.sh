#!/bin/bash

clonee=glen-base
guest=foster-base
. helper_funcs.sh

lxc_clone
wait_for_guest

sudo -u phil env guest=${guest} bash -x <<EOF1
ssh-keygen -f "/home/phil/.ssh/known_hosts" -R ${guest}
ssh ${guest} sudo bash -x <<EOF2
apt-get install -y software-properties-common
add-apt-repository ppa:x2go/stable
apt-get update < /dev/null
apt-get upgrade -y < /dev/null
apt-get install -y x2goserver < /dev/null
apt-get install -y xubuntu-desktop < /dev/null
apt-get install -f
EOF2
EOF1

lxc-stop -n ${guest}
