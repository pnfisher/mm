whoami=`whoami`
error () {
  echo "error, " $1
  exit 1
}

[ "$whoami" == "root" ] || error "must be root"
[ -z "$guest" ] && error "guest not defined"

file_check() {
  if [ ! -f $1 ]; then
    error "$1 missing"
  fi
}

wait_for_guest() {

  t=$1
  [ -z "$t" ] && t=10

  echo waiting $t seconds for ${guest} to start
  for((i=0;i<t;i++)); do
    ping -c 1 -W 1 ${guest} >& /dev/null
    case $? in
      0) break;;
      1) ;;
      *) sleep 1;;
    esac
    echo -n .
  done

  echo

  if((i==t)); then
    error "${guest} not responding to ping"
  fi

}

lxc_create() {

  set -x
  lxc-stop -n ${guest} >& /dev/null
  lxc-destroy -n ${guest}
  lxc-create -n ${guest} -t ubuntu -- -r trusty -b phil
  [ $? == 0 ] || error "lxc-create failed"
  set +x

}

lxc_clone() {

  [ -z "$clonee" ] && error "clonee not defined"
  file_check /var/lib/lxc/${clonee}/config

  set -x
  lxc-stop -n ${guest} >& /dev/null
  lxc-destroy -n ${guest}
  lxc-clone -B overlayfs -o ${clonee} -n ${guest} -s
  #lxc-clone -o ${clonee} -n ${guest}
  [ $? == 0 ] || error "lxc-clone failed"
  lxc-start -n ${guest} -d
  [ $? == 0 ] || error "lxc-start failed"
  set +x

}
