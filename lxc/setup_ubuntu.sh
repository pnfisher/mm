#!/bin/bash

guest=ubuntu-base
lxcdir=/var/lib/lxc/${guest}
. helper_funcs.sh

lxc_create

set -x
cfgfile=${lxcdir}/config
file_check ${cfgfile}

rm -f ${cfgfile}.tmp
mv ${cfgfile} ${cfgfile}.tmp
cat ${cfgfile}.tmp | sed 's/lxcbr0/br0/' >> ${cfgfile}
rm -f ${cfgfile}.tmp

aptcache=/var/cache/lxc/apt/archives
mkdir -p ${aptcache}
rsync -av ${lxcdir}/rootfs/var/cache/apt/archives/ ${aptcache}/

fstab=${lxcdir}/fstab
file_check ${fstab}
echo "${aptcache} var/cache/apt/archives none bind 0 0" >> ${fstab}

sudoers=${lxcdir}/rootfs/etc/sudoers
file_check ${sudoers}
echo "phil ALL=(ALL) NOPASSWD: ALL" >> ${sudoers}

aptsources=${lxcdir}/rootfs/etc/apt/sources.list
file_check ${aptsources}
cat ${aptsources} | sed 's/^deb/deb-src/g' >> ${aptsources}

lxc-start -n ${guest} -d
[ $? == 0 ] || error "lxc-start error"
set +x

wait_for_guest

sudo -u phil env guest=${guest} bash -x <<EOF1
ssh-keygen -f "/home/phil/.ssh/known_hosts" -R ${guest}
ssh ${guest} sudo bash -x <<EOF2
apt-get update
apt-get -y upgrade
EOF2
EOF1

lxc-stop -n ${guest}


