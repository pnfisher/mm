#!/bin/bash

clonee=ubuntu-base
guest=glen-base
. helper_funcs.sh

lxc_clone
wait_for_guest

sudo -u phil env guest=${guest} bash -x <<EOF1
ssh-keygen -f "/home/phil/.ssh/known_hosts" -R ${guest}
ssh ${guest} sudo bash -x <<EOF2
apt-get update
apt-get upgrade
apt-get install -y \
software-properties-common \
xorg libgtk2.0-0 \
xterm emacs xaw3dg \
python python3 \
build-essential
EOF2
EOF1

lxc-stop -n ${guest}


