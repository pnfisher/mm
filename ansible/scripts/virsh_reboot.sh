#!/bin/bash

# ubuntu 15.04 with systemd has bugging behaviour when calling
# shutdown that messes up ansible; we need to work around it

timeout=30

[ -z "$1" ] && {
  echo "missing host argument" 1>&2;
  exit 1;
}
host="libvirt_$1"

virsh list --all --state-running | grep ${host} >& /dev/null
(($? != 0)) && {
  echo "virsh can't find any host named $host" 1>&2;
  exit 1;
}

for((i=0;i<timeout;i++)); do
  set -x
  virsh shutdown ${host} >& /dev/null
  (($? != 0)) && break
  sleep 1
  set +x
done

((i==timeout)) && {
  echo "couldn't shutdown ${host} (timeout)" 1>&2;
  exit 1;
}

virsh start ${host} >& /dev/null
(($? != 0)) && {
  echo "virsh start $host failed" 1>&2;
  exit 1;
}

for((i=0;i<timeout;i++)); do
  virsh list --all --state-running | grep ${host} >& /dev/null
  (($? == 0)) && break
  sleep 1
done

((i==timeout)) && {
  echo "couldn't start ${host} (timeout)" 1>&2;
  exit 1;
}
