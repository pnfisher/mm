#!/bin/bash

. ~/.profile

deps=\
$(ldd `which emacs` | \
grep "not found" | \
awk '{print $1}' | \
sed 's/[-_.].*//g')

packages=

for d in ${deps}; do

  if [ "${d}" == "libXaw3d" ]; then
    packages="${packages} xaw3dg"
    continue
  fi

  if [ "${d}" == "libgio" ]; then
    packages="${packages} libgio-cil"
    continue
  fi

  if [ "${d}" == "libgobject" ]; then
    packages="${packages} libglib2.0-0"
    continue
  fi

  if [[ ${d} =~ libMagickWand.* ]]; then
    packages="${packages} libmagickwand5"
    continue
  fi

  if [[ ${d} =~ libMagickCore.* ]]; then
    packages="${packages} libmagickcore5"
    continue
  fi

  needed=$(apt-cache depends emacs24 | \
           grep ${d} | \
           awk '{print $2}' 2> /dev/null)
  if [ -z "${needed}" ]; then
    echo "error, couldn't find dependency for $d" 1>&2
    exit 1
  fi

  packages="${packages} ${needed}"

done

# must be output one package per line
for p in ${packages}; do
  echo $p
done

