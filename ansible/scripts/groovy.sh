#!/bin/bash

if (($# != 1)); then
  echo "error, missing directory argument" 1>&2
  exit 1
fi

cd $1 || {
  echo "error, $1 directory missing" 1>&2;
  exit 1;
}

for f in $(ls lib/groovy*.jar | cut -d/ -f2); do
  k=$(basename $f .jar)
  [ ! -f lib/$k.jar.old ] && mv lib/$k.jar lib/$k.jar.old
  cmp indy/$k-indy.jar lib/$k.jar >& /dev/null
  (($? != 0)) && cp indy/$k-indy.jar lib/$k.jar
done
