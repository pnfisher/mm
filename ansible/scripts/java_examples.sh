#!/bin/bash

if (($# != 2)); then
  echo "error, download and unzip directories not specified" 1>&2
  exit 1
fi

downdir=$1
mkdir -p ${downdir}
javadir=$2
mkdir -p ${javadir}

while read http; do

  zip=$(basename ${http})
  if [ ! -f ${downdir}/${zip} ]; then
    wget -q ${http} -P ${downdir}
    if (($? != 0)); then
      echo "error, wget ${zip} failed" 1>&2
      exit 1
    fi
  fi

  dir=${javadir}/$(echo ${zip} | cut -d . -f 1)
  [ -d ${dir} ] && continue

  mkdir -p ${dir}
  unzip ${downdir}/${zip} -d ${dir}
  if (($? != 0)); then
    echo "error, unzip ${downdir}/${zip} failed" 1>&2
    exit 1
  fi

done <<EOF
http://www.horstmann.com/corejava/corejava.zip
http://www.mhprofessional.com/downloads/products/0071837345/javaee7code.zip
http://examples.oreilly.com/9780596007737/Javanut5Examples.zip
http://jcip.net.s3-website-us-east-1.amazonaws.com/jcip-examples-src.jar
EOF


