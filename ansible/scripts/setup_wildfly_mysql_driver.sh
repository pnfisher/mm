#!/bin/bash

if [ $# != 1 ]; then
  echo "error, missing wildfly directory argument" 1>&2
  exit 1
fi

wfdir=$1

if [ ! -d ${wfdir} ]; then
  echo "error, ${wfdir} doesn't exist"
  exit 1
fi

cd ${wfdir} || exit 1

./standalone.sh >& /dev/null &

rc=0
for((i=0;i<10;i++)); do
  result=$(./jboss-cli.sh --connect '/subsystem=datasources/jdbc-driver=mysql:add(driver-name=mysql, driver-module-name=com.mysql, driver-class-name=com.mysql.jdbc.Driver)')
  rc=$?
  [ $rc == 0 ] && break
  if [[ $result =~ ^Failed.to.connect.to.the.controller: ]]; then
    sleep 1
    continue
  fi
  break
done

./jboss-cli.sh --connect shutdown >& /dev/null

if (($rc != 0)); then
  echo "error, attempt to configure mysql driver failed" 1>&2
  echo "$result" 1>&2
  exit 1
fi


