#!/bin/bash

cfg=$(find `pwd`/ -type f -name ".ansible.cfg")
[ -z "${cfg}" ] && echo "error, couldn't find .ansible.cfg file" && exit 1

ln -sf ${cfg} /home/phil/.ansible.cfg
