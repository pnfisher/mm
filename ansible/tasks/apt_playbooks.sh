#!/bin/bash

aptpkgs=apt_packages.yml

if [ ! -f ${aptpkgs} ]; then
  echo "error, ${aptpkgs} is missing" 1>&2
  exit 1
fi

begin="    ## apt_packages.sh vars begin ##"
end="    ## apt_packages.sh vars end ##"
IFS=$'\n'

tfile=$(mktemp)

for playbook in ../*.yml; do

  # skip playbooks that don't have our apt_packages.yml variable begin
  # and end markers
  grep -n "${begin}" ${playbook} >& /dev/null
  (($? != 0)) && continue
  grep "${end}" ${playbook} >& /dev/null
  (($? != 0)) && continue

  echo processing $(basename ${playbook})

  rm -f ${tfile}
  touch ${tfile}

  # get the pre apt_package.yml variable stuff from the real playbook
  # into our temp file
  while read line; do
    [ "$line" == "${begin}" ] && break
    echo ${line} >> ${tfile}
  done < ${playbook}

  # get the apt_package.yml variables into our temp file
  i=
  while read line; do
    [ "$line" == "${begin}" ] && i=0
    [ "$line" == "${end}" ] && break
    [ -n "$i" ] && {
      echo ${line} >> ${tfile};
      aptpkg_vars[$((i++))]=${line};
    }
  done < ${aptpkgs}

  # get the post apt_package.yml variable stuff from the real playbook
  # into our temp file
  i=
  while read line; do
    [ "$line" == "${end}" ] && i=1
    [ -n "$i" ] && echo ${line} >> ${tfile}
  done < ${playbook}

  # go through the apt_package.yml variable settings in our real
  # playbook one by one and sub into our temp file if they are
  # mentioned in our temp file
  i=
  while read line; do

    [ "$line" == "${begin}" ] && i=1
    [ "$line" == "${end}" ] && break
    [ -z "$i" ] && continue
    [[ $line =~ : ]] || continue

    package=$(echo $line | sed 's/[ #\t]*\(.*\):.*/\1/g')
    [[ "${aptpkg_vars[@]}" =~ "# ${package}: " ]] || continue
    #egrep "# ${package}: " ${tfile} >& /dev/null
    #(($? != 0)) && continue

    # override temp file settings with playbook setting if temp file
    # has an entry for the playbook package setting in question
    sed -i "s/.*\(#\| \|\t\)${package}:.*/${line}/" ${tfile}

  done < ${playbook}

  cp ${tfile} ${playbook}
  #cat ${tfile}
  rm -f ${tfile}

done
