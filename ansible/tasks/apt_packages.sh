#!/bin/bash

pkg_list=apt_packages.list

if [ ! -f ${pkg_list} ]; then
  echo "error, ${pkg_list} missing" 1>&2
  exit 1
fi

echo "## variables"
echo
echo "    ################################"
echo "    ## apt_packages.sh vars begin ##"
echo "    ################################"

while read key value; do

  if [ "$key" == "include:" ];then

    if [ ! -f ${value}.yml ]; then
      echo "error, ${value}.yml missing" 1>&2
      exit 1
    fi

    value=$(echo ${value} | sed 's/-/_/g')

    echo "    # ${value}: true"
    continue

  fi

  if [ "$key" == "package:" ]; then
    value=$(echo ${value} | sed 's/-/_/g')
    echo "    # ${value}: true"
    continue
  fi

  if [ "$key" == "variable:" ]; then
    if [[ $value =~ ^v_ ]]; then
      echo "    # ${value}: true"
      continue
    fi
    echo "error, ${value} is an illegal global variable name" 1>&2
    exit 1
  fi

  if [ -z "$key" -a -z "$value" ]; then
    continue
  fi

  if [[ $key =~ ^#.* ]]; then
    echo -n "    $key"
    [ -n "$value" ] && echo -n " $value"
    echo
    continue
  fi

  echo "error, ${key} is not a recognized key" 1>&2
  exit 1

done < ${pkg_list}

echo "    ##############################"
echo "    ## apt_packages.sh vars end ##"
echo "    ##############################"
echo
echo "## packaging"
echo

while read key value; do

  if [ "$key" == "include:" ];then

    if [ ! -f ${value}.yml ]; then
      echo "error, ${value}.yml missing" 1>&2
      exit 1
    fi

    echo "- include: ${value}.yml"

    value=$(echo ${value} | sed 's/-/_/g')

    echo "  when:"
    echo "    ${value} is defined and"
    echo "    ${value}"
    echo

    continue

  fi

  if [ "$key" == "package:" ]; then

    echo "- name: install ${value}"
    echo "  apt: pkg=${value} state=latest"

    value=$(echo ${value} | sed 's/-/_/g')

    echo "  when:"
    echo "    ${value} is defined and"
    echo "    ${value}"
    echo

    continue

  fi

  if [ "$key" == "variable:" ]; then
    continue
  fi

  if [ -z "$key" -a -z "$value" ]; then
    echo
    continue
  fi

  if [[ $key =~ ^#.* ]]; then
    echo -n $key
    [ -n "$value" ] && echo -n " $value"
    echo
    continue
  fi

  echo "error, ${key} is not a recognized key" 1>&2
  exit 1

done < ${pkg_list}
