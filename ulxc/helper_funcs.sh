error() {
  echo "error," $* 1>&2
  exit 1
}

if [ -z "${machine}" ]; then
  [ $# == 0 ] && error "missing lxc machine name"
  machine=$1
fi

prep_basemachine() {
  lxc-info -n ${base_machine} >& /dev/null
  if [ $? != 0 ]; then
    if [[ $base_machine =~ (trusty|vivid|xenial)_(i386|amd64) ]]; then
      echo "creating lxc ${base_machine}"
      LXC_RELEASE=${BASH_REMATCH[1]} LXC_ARCH=${BASH_REMATCH[2]} \
        ./ulxc_createbase.sh
      if [ $? != 0 ]; then
        echo "error, couldn't create lxc ${base_machine}" 1>&2
        ./ulxc_destroy.sh ${base_machine} >& /dev/null
        exit 1
      fi
    else
      echo "error, lxc ${base_machine} does not exist and" 1>&2
      echo "cannot be automatically created" 1>&2
      exit 1
    fi
  fi
  lxc-wait -n ${base_machine} -s RUNNING -t 0 >& /dev/null
  if [ $? == 0 ]; then
    lxc-stop -n ${base_machine}
    [ $? != 0 ] && error "couldn't stop lxc ${base_machine}"
    lxc-wait -n ${base_machine} -s STOPPED -t 30
    [ $? != 0 ] && error "couldn't stop lxc ${base_machine}"
  fi
}

lxc_ip() {

  local IP

  IP=$(lxc-info -n ${machine} -i -H)
  if [ $? != 0 ]; then
    echo
    return
  fi

  echo $IP

}

wait_for_lxc() {

  t=${1:-10}

  echo waiting $t seconds for lxc ${machine} to start

  for((i=0;i<t;i++)); do

    IP=$(lxc_ip)
    if [ -z "$IP" ]; then
      echo -n .
      sleep 1
      continue
    fi

    ping -c 1 -W 1 ${IP} >& /dev/null
    case $? in
      0) break;;
      1) ;;
      *) sleep 1;;
    esac
    echo -n .
  done

  echo

  ((i==t)) && error "${machine} not responding to ping"

}

lxc_start() {
  LXCWASRUNNING=1
  if ! lxc-wait -n ${machine} -s RUNNING -t 0; then
    echo starting lxc ${machine}
    lxc-start -n ${machine} -d
    lxc-wait -n ${machine} -s RUNNING -t ${1:-10}
    (($? != 0)) && error "couldn't start lxc ${machine}"
    [ -n "$1" ] && wait_for_lxc $1
    LXCWASRUNNING=0
  else
    echo lxc ${machine} is already running
  fi
}

# lxc_start() {
#   echo starting ${machine}
#   lxc-start -n ${machine} -d
#   if [ $? != 0 ]; then
#     error "lxc-start failed"
#   fi
#   if [ -n "$1" ]; then
#     wait_for_lxc $1
#   fi
# }

lxc_stop() {
  echo stopping lxc ${machine}
  lxc-wait -n ${machine} -s STOPPED -t 0
  if (($? != 0)); then
    lxc-stop -n ${machine}
    [ $? != 0 ] && error "failed to stop lxc ${machine}"
    lxc-wait -n ${machine} -s STOPPED -t 30
    [ $? != 0 ] && error "couldn't stop lxc ${machine}"
  fi
  echo lxc ${machine} is stopped
}

lxc_attach() {
  echo lxc-attach -n ${machine} -- $*
  lxc-attach -n ${machine} -- bash -c "$*"
  [ $? != 0 ] && error "lxc-attach failed"
}

lxc_update() {
  echo lxc-attach -n ${machine} -- apt-get update
  lxc-attach -n ${machine} -- <<EOF
apt-get update < /dev/null
EOF
  [ $? != 0 ] && error "apt-get update failed"
}

lxc_upgrade() {
  echo lxc-attach -n ${machine} -- apt-get -y upgrade
  lxc-attach -n ${machine} -- <<EOF
apt-get -y upgrade < /dev/null
EOF
  [ $? != 0 ] && error "apt-get upgrade failed"
}

lxc_install() {
  echo lxc-attach -n ${machine} -- apt-get install -y $*
  lxc-attach -n ${machine} -- <<EOF
apt-get install -y $* < /dev/null
EOF
  [ $? != 0 ] && error "apt-get install failed"
}
