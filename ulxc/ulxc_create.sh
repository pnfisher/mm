#!/bin/bash

if (($# < 1)); then
  echo "usage: ulxc create base_machine [new_machine]" 1>&2
  exit 1
fi

. helper_funcs.sh

base_machine=$1
machine=$2
[ "${base_machine}" == "${machine}" ] && \
  error "you can't create a clone of lxc ${machine} from lxc ${machine}"

if [ -n "${machine}" ]; then
  lxc-info -n ${machine} >& /dev/null
  if [ $? == 0 ]; then
    echo lxc ${machine} already exists
    exit 0
  fi
fi

prep_basemachine
if [ -z "${machine}" ]; then
  lxc-info -n ${base_machine}
  exit 0
fi

ssh-keygen -f "${HOME}/.ssh/known_hosts" -R ${machine} >& /dev/null

echo creating lxc ${machine} by copying lxc ${base_machine}
echo lxc-clone -o ${base_machine} -n ${machine} -B dir
lxc-clone -o ${base_machine} -n ${machine} -B dir
[ $? != 0 ] && error "clone of lxc ${base_machine} failed"

./ulxc_provision.sh ${machine}
exit $?
