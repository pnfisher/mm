#!/bin/bash

if (($# != 2)); then
  echo "two arguments required: [cloned machine] [new machine]" 1>&2
  exit 1
fi

. helper_funcs.sh

base_machine=$1
machine=$2
[ "${base_machine}" == "${machine}" ] && \
  error "you can't create a clone of lxc ${machine} from lxc ${machine}"

lxc-info -n ${machine} >& /dev/null
if [ $? == 0 ]; then
  echo lxc ${machine} already exists
  exit 0
fi

prep_basemachine

ssh-keygen -f "${HOME}/.ssh/known_hosts" -R ${machine} >& /dev/null

echo creating lxc ${machine} by cloning lxc ${base_machine}
echo lxc-clone -o ${base_machine} -n ${machine} -B overlayfs -s
lxc-clone -o ${base_machine} -n ${machine} -B overlayfs -s
[ $? != 0 ] && error "clone of ${base_machine} failed"

./ulxc_provision.sh ${machine}
exit $?
