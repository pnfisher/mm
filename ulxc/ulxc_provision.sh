#!/bin/bash

. helper_funcs.sh

lxc-info -n ${machine} >& /dev/null
[ $? != 0 ] && error "${machine} does not exist"

playbook=../ansible/${machine}.yml
if [ ! -f ${playbook} ]; then
  echo "can't find playbook for lxc ${machine}; skipping further provisioning"
  exit 0
fi

lxc_start 30
IP=$(lxc_ip)
echo "provisioning with ansible using ${playbook} playbook"
#ssh ${machine} xterm -e tail -f /var/log/dpkg.log &
#pid=$!
cd ../ansible || exit 1
ANSIBLE_FORCE_COLOR=1 \
ansible-playbook ${machine}.yml \
 -e "ansible_ssh_host=${IP} \
     ansible_ssh_user=${USER}" \
 -i hosts
rc=$?
[ ${LXCWASRUNNING:-0} == 0 ] && lxc-stop -n ${machine}
#kill ${pid}
[ $rc != 0 ] && error "ansible provisioning failed"
exit $rc
