#!/bin/bash

echo \#
echo \# machine stats
echo \#
lxc-ls -f
echo
echo \#
echo \# disk usage
echo \#
lxc_share=${HOME}/.local/share/lxc
lxc_cache=${HOME}/.cache/lxc
du -csh ${lxc_share}/* ${lxc_cache}/* 2> /dev/null

