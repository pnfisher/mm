#!/bin/bash -x

error() {
  error "" $* 1>&2
  exit 1
}

LXCHOST=xalibu
DISPLAY=:1
STARTED=false
PULSE=${HOME}/tmp/pulse/${LXCHOST}/.pulse_socket
LOG=${HOME}/tmp/startx.log

if [ "$1" == "--start-session" ]; then
  xhost +
  ssh ${LXCHOST} bash <<EOF
rm -f ${LOG}
export DISPLAY=${DISPLAY}
if [ -e ${PULSE} ]; then
  echo "INFO: ${PULSE} found; starting pulse server"
  export PULSE_SERVER=${PULSE}
else
  echo "INFO: ${PULSE} not found; skipping pulse server start"
fi
/usr/bin/xfce4-session >& ${LOG}
EOF
  exit $?
  #"export DISPLAY=${DISPLAY} PULSE_SERVER=${PULSE}; \
  #/usr/bin/xfce4-session >& ${LOG}"
  #rm -f ${LOG}
  #exit $?
fi

if ! lxc-wait -n ${LXCHOST} -s RUNNING -t 0; then
  lxc-start -n ${LXCHOST} -d
  [ $? != 0 ] && error "couldn't start ${LXCHOST}"
  lxc-wait -n ${LXCHOST} -s RUNNING -t 30
  [ $? != 0 ] && error "couldn't start ${LXCHOST}"
  STARTED=true
fi

sleep 2
startx \
    /usr/bin/xterm \
    -fn 6x10 \
    -T xubuntu@${LXCHOST} \
    -e $(pwd)/$0 --start-session \
    -- ${DISPLAY}

if [ "$STARTED" == "true" ]; then
  lxc-stop -n ${LXCHOST} -t 10
fi
