#!/bin/bash

error() {
  echo "error, " $* 1>&2
  exit 1
}

nameserver="10.0.3.1"
subid_begin="100000"
subid_num="65536"
subid_end=$((subid_begin+subid_num-1))
veth_lxcbr0="${USER} veth lxcbr0 10"
veth_br0="${USER} veth br0 10"

run_ansible() {

  echo "sorry, we need to set some things with sudo"

  playbook=$(mktemp)
  cat <<EOF > ${playbook}
- hosts: local
  sudo: yes

  tasks:

    - name: tweak /etc/subuid
      command: "usermod --add-subuids ${subid_begin}-${subid_end} ${USER}"

    - name: tweak /etc/subgid
      command: "usermod --add-subgids ${subid_begin}-${subid_end} ${USER}"

    - name: tweak /etc/lxc/lxc-usernet (br0)
      lineinfile:
        line: ${veth_br0}
        dest: /etc/lxc/lxc-usernet
        state: present

    - name: tweak /etc/lxc/lxc-usernet (lxcbr0)
      lineinfile:
        line: ${veth_lxcbr0}
        dest: /etc/lxc/lxc-usernet
        state: present

    - name: tweak /etc/resolvconf/resolv.conf.d/head
      lineinfile:
        line: "nameserver ${nameserver}"
        dest: /etc/resolvconf/resolv.conf.d/head
        state: present
        create: yes
        owner: root
        group: root
        mode: 0644
      register: resolv

    - name: run resolvconf
      command: resolvconf -u
      when: resolv.changed
EOF

  inventory=$(mktemp)
  cat <<EOF > ${inventory}
[targets]
local ansible_connection=local
EOF

  ansible-playbook ${playbook} -i ${inventory} -K
  rc=$?
  rm -f ${playbook} ${inventory}
  (($rc != 0)) && error "ansible failed"
  return $rc

}

dpkg -s ansible >& /dev/null
(($? != 0)) && error "ansible not installed"

[ ! -d ../ansible ] && error "can't find ansible directory"

grep "${USER}:${subid_begin}:${subid_num}" /etc/subuid >& /dev/null
if [ $? != 0 ]; then
  run_ansible
  exit $?
fi

grep "${USER}:${subid_begin}:${subid_num}" /etc/subgid >& /dev/null
if [ $? != 0 ]; then
  run_ansible
  exit $?
fi

grep "${veth_br0}" /etc/lxc/lxc-usernet >& /dev/null
if [ $? != 0 ]; then
  run_ansible
  exit $?
fi

grep "${veth_lxcbr0}" /etc/lxc/lxc-usernet >& /dev/null
if [ $? != 0 ]; then
  run_ansible
  exit $?
fi

grep "nameserver ${nameserver}" /etc/resolvconf/resolv.conf.d/head >& /dev/null
if [ $? != 0 ]; then
  run_ansible
  exit $?
fi
