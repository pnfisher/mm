#!/bin/bash

. helper_funcs.sh

lxc-info -n ${machine} >& /dev/null
[ $? != 0 ] && echo error, lxc ${machine} does not exist

lxc-info -n ${machine} | egrep "^State:.*RUNNING" >& /dev/null
[ $? == 0 ] && \
    error "lxc ${machine} is running, please stop it and rerun"

[ -z "$2" ] && error "missing network link argument [br0|lxcbr0]"
[ "$2" != "br0" -a "$2" != "lxcbr0" ] && \
    error "illegal network link argument, not [br0|lxcbr0]"

configfile=/home/${USER}/.local/share/lxc/${machine}/config
[ ! -f ${configfile} ] &&
    error "${configfile} for lxc ${machine} missing"

playbook=$(mktemp)
cat <<EOF > ${playbook}
- hosts: local
  sudo: no

  tasks:

    - name: add lxcbr0 network link to lxc ${machine} config file
      lineinfile:
        line: "lxc.network.link = lxcbr0"
        dest: ${configfile}
        state: present
      when: lxc_netlink == "lxcbr0"

    - name: rm br0 network link from lxc ${machine} config file
      lineinfile:
        line: "lxc.network.link = br0"
        dest: ${configfile}
        state: absent
      when: lxc_netlink == "lxcbr0"

    - name: add br0 network link to lxc ${machine} config file
      lineinfile:
        line: "lxc.network.link = br0"
        dest: ${configfile}
        state: present
      when: lxc_netlink == "br0"

    - name: rm lxcbr0 network link from lxc ${machine} config file
      lineinfile:
        line: "lxc.network.link = lxcbr0"
        dest: ${configfile}
        state: absent
      when: lxc_netlink == "br0"
EOF

inventory=$(mktemp)
cat <<EOF > ${inventory}
[targets]
local ansible_connection=local
EOF

ansible-playbook ${playbook} -i ${inventory} -e lxc_netlink=${2}
rc=$?
rm -f ${playbook} ${inventory}
[ $rc != 0 ] && error "ansible failed"
