. /etc/lsb-release || {
  echo "error, couldn't source /etc/lsb-release" 1>&2;
  exit 1;
}

distro=ubuntu
release=${LXC_RELEASE:-${DISTRIB_CODENAME}}
arch=${LXC_ARCH:-"amd64"}
base_machine=${LXC_MACHINE:-${release}_${arch}}

if [ ! -d ../ansible ]; then
  echo "error, ansible directory missing" 1>&2
  exit 1
fi

if [ ! -f ../ansible/${base_machine}.yml ]; then
  echo "error, ${base_machine}.yml playbook missing" 1>&2
  exit 1
fi
