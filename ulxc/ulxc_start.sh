#!/bin/bash

. helper_funcs.sh

lxc-info -n ${machine} >& /dev/null
[ $? != 0 ] && error "lxc ${machine} does not exist"
lxc_start 30
