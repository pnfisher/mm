#!/bin/bash

[ $# != 0 ] && error "no arguments allowed"

network_link="lxcbr0"
#network_link="br0"

. base_distro.sh
machine=${base_machine}
. helper_funcs.sh

# sanity check
[ -z "${machine}" ] && error "machine not defined"
[ "${machine}" != "${base_machine}" ] && \
    error "base_machine incorrectly defined"

lxc-info -n ${machine} >& /dev/null
if [ $? == 0 ]; then
  echo lxc ${machine} already exists
  exit 0
fi

timeout 4s ssh $(hostname) exit >& /dev/null
if [ $? != 0 ]; then
  echo "couldn't ssh to $(hostname) without password" 1>&2
  echo \
    "have you run 'ssh-copy-id -i ~/.ssh/id_rsa $(hostname)' on $(hostname)?"
  exit 1
fi

spfile="shadowpass.txt"
shadowpass=$(cat ${spfile} 2> /dev/null | tail -1)
if [ -z "${shadowpass}" ]; then
  while true; do
    read -s -p "Password for ${USER} on ${machine}: " p1
    echo
    read -s -p "Reenter password for ${USER} on ${machine}: " p2
    echo
    [ "$p1" == "$p2" -a -n "$p1" ] && break
    echo "mismatch, please try again"
  done
  rm -f ${spfile}
  echo "# mkpasswd password -m sha-512 | sed 's/\//\\\//g'" > ${spfile}
  chmod 0600 ${spfile}
  mkpasswd "${p1}" -m sha-512 | sed 's/\//\\\//g' >> ${spfile}
  shadowpass=$(cat ${spfile} | tail -1)
  [ $? != 0 -o -z "${shadowpass}" ] && \
      error "couldn't figure out shadow password"
fi

./setup_localhost.sh
(($? != 0)) && error "setup_localhost.sh failed"

lxc_cfg=${HOME}/.config/lxc
lxc_share=${HOME}/.local/share/lxc
lxc_cache=${HOME}/.cache/lxc
aptcache=${lxc_cache}/apt/archives

mkdir -p ${lxc_cfg}
mkdir -p ${lxc_share}
mkdir -p ${lxc_cache}
mkdir -p ${aptcache}

chmod +x ${HOME}

dconf=${lxc_cfg}/default.conf
fstab=${lxc_share}/${machine}/fstab
GID=$(id -g)

rm -f ${dconf}
cat <<EOF > ${dconf}
lxc.start.auto = 0
lxc.network.type = veth
lxc.network.link = ${network_link}
lxc.network.flags = up
lxc.network.hwaddr = 00:16:3e:xx:xx:xx
lxc.id_map = u 0 100000 ${UID}
lxc.id_map = g 0 100000 ${GID}
lxc.id_map = u ${UID} ${UID} 1
lxc.id_map = g ${GID} ${GID} 1
lxc.id_map = u $((UID+1)) $((100000+UID)) 64536
lxc.id_map = g $((GID+1)) $((100000+GID)) 64536
lxc.mount = ${fstab}
#lxc.mount.entry = /dev/dri dev/dri none bind,optional,create=dir
#lxc.mount.entry = /dev/snd dev/snd none bind,optional,create=dir
#lxc.mount.entry = /tmp/.X11-unix tmp/.X11-unix none bind,optional,create=dir
#lxc.mount.entry = /dev/video0 dev/video0 none bind,optional,create=file
#lxc.hook.pre-start = /home/USERNAME/.local/share/lxc/precise-gui/setup-pulse.sh
EOF

ssh-keygen -f "${HOME}/.ssh/known_hosts" -R ${machine} >& /dev/null

echo \
  lxc-create -t download -n $machine -- -d ${distro} -r ${release} -a ${arch}
lxc-create -t download -n $machine -- -d ${distro} -r ${release} -a ${arch}
[ $? != 0 ] && error "lxc-create failed"

rm -f ${fstab}
echo "${aptcache} var/cache/apt/archives none bind 0 0" >> ${fstab}
[ $? != 0 ] && error "couldn't create ${fstab}"

lxc_start 30

echo "beginning preliminary setup for lxc ${machine}"

lxc-attach \
    -n ${machine} -- \
    env \
    MACHINE=${release} \
    USER=${USER} \
    HOME=${HOME} \
    PASS=${shadowpass} \
    UID=${UID} \
    GID=${GID} \
    bash <<\EOF
user=$(getent passwd ${UID})
if (($? == 0)); then
  # delete user using uid ${UID}
  user=$(echo $user | cut -d : -f 1)
  if [ -n "$user" ]; then
    echo deleting user $user with uid ${UID}
    userdel $user || exit 1
  fi
fi
cat /etc/group | grep ":${GID}:" >& /dev/null
[ $? = 0 ] || groupadd -f -g ${GID} ${USER}
set -ve
useradd -m -d ${HOME} -s /bin/bash -G sudo --uid ${UID} --gid ${GID} ${USER}
#echo ${USER}:${USER} | chpasswd
sed -i "s/^${USER}:[^:]*:\(.*\)$/${USER}:${PASS}:\1/g" /etc/shadow
echo ${USER} ALL=\(ALL\) NOPASSWD: ALL >> /etc/sudoers
cat /etc/apt/sources.list | sed 's/^deb/deb-src/g' >> /etc/apt/sources.list
echo \
deb http://archive.ubuntu.com/ubuntu ${MACHINE}-backports \
main restricted universe multiverse >> /etc/apt/sources.list
echo \
deb-src http://archive.ubuntu.com/ubuntu ${MACHINE}-backports \
main restricted universe multiverse >> /etc/apt/sources.list
# update and upgrade
apt-get update && apt-get -y upgrade < /dev/null
# install openssh (so we can ssh in)
apt-get install -y openssh-server < /dev/null
# because our uid and gid mappings have a hole in them for ${UID}/${GID}
sed -i 's/\(UsePrivilegeSeparation\) yes/\1 no/g' /etc/ssh/sshd_config
# ansible target host requirements
apt-get install -y python < /dev/null
apt-get install -y python-apt < /dev/null
# hush up virtualenvwrapper stuff when we ssh in
#wget https://bitbucket.org/pypa/setuptools/raw/bootstrap/ez_setup.py -O - | \
#sudo python
EOF
rc=$?
lxc_stop
(($rc != 0)) && \
    error "preliminary setup of lxc ${machine} using lxc-attach failed"

# setup rest of fstab now that user ${USER} exists
echo "setting up fstab for lxc ${machine}"
phil_mntpoints=../ansible/files/phil_mntpoints
[ -f ${phil_mntpoints} ] || error "${phil_mntpoints} is missing"
for d in $(cat ${phil_mntpoints}); do
  echo /home/${USER}/${d} home/${USER}/${d} none bind,create=dir 0 0 >> \
       ${fstab}
done
cat <<EOF >> ${fstab}
#/home/${USER} home/${USER} none bind 0 0
/dev/dri dev/dri none bind,optional,create=dir
/dev/snd dev/snd none bind,optional,create=dir
/tmp/.X11-unix tmp/.X11-unix none bind,optional,create=dir
/dev/video0 dev/video0 none bind,optional,create=file
EOF
[ $? != 0 ] && error "couldn't append to ${fstab}"
cat ${fstab}

#
# further provisioning with ansible
#

./ulxc_provision.sh ${machine}
exit $?
