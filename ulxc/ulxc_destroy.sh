#!/bin/bash

. helper_funcs.sh

[ "${machine}" == "all" ] && machine=$(lxc-ls)

for m in ${machine}; do

  lxc-info -n ${m} >& /dev/null
  (($? != 0)) && error "lxc ${m} doesn't exist"

  lxc-info -n ${m} | egrep "^State:.*RUNNING" >& /dev/null
  if [ $? == 0 ]; then
    echo "stopping lxc ${m}"
    lxc-stop -n ${m} >& /dev/null
    (($? != 0)) && error "couldn't stop lxc ${m}"
  fi
  echo "destroying lxc ${m}"
  lxc-destroy -n ${m} >& /dev/null
  (($? != 0)) && error "couldn't destroy lxc ${m}"

done
