#!/bin/bash

. base_distro.sh
. helper_funcs.sh

lxc-info -n ${machine} >& /dev/null
if [ $? == 0 ]; then
  lxc-info -n ${machine} | egrep "^State:.*RUNNING" >& /dev/null
  if [ $? == 0 ]; then
    echo "lxc ${machine} already running"
    exit 0
  fi
else
  echo "${machine} doesn't exist"
  while true; do
    read -p \
      "create lxc ${machine} as [c]opy or [s]napshot of lxc ${base_machine}: " \
      response
    [ "$response" == "c" -o "$response" == "s" ] && break
    echo "incorrect response, please specify 's' or 'c'"
  done
  if [ "$response" == "c" ]; then
    echo "creating lxc ${machine} as copy of lxc ${base_machine}"
    ./ulxc_create.sh ${machine}
    rc=$?
  else
    echo "creating lxc ${machine} as snapshot clone of lxc ${base_machine}"
    ./ulxc_clone.sh ${base_machine} ${machine}
    rc=$?
  fi
  (($rc != 0)) && error "lxc ${machine} clone/create encountered an error"
fi

lxc_start 30

