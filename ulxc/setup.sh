#!/bin/bash

src=$(pwd)
cd ~/bin || { echo "can't find ~/bin 1>&2"; exit 1; }
ln -sf ${src}/ulxc.sh ulxc
