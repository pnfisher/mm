#!/bin/bash

error () {
  echo "error," $* 1>&2
  exit 1
}

dpkg -s lxc >& /dev/null
(($? != 0)) && error "lxc is not installed"

dir=${HOME}/mm/ulxc
cd ${dir} || error "couldn't find ${dir} directory"

if (($# == 0)); then
  echo "error, missing command argument" 1>&2
  echo "supported commands: " 1>&2
  for f in ulxc_*.sh; do
    [ ! -x $f ] && continue
    cmd=${f#ulxc_}
    echo "  ${cmd%.sh}" 1>&2
  done
  exit 1
fi

cmd=$1
shift

[ -x ulxc_${cmd}.sh ] || error "'${cmd}' is an unsupported command"

./ulxc_${cmd}.sh $*
